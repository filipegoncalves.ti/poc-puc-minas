from django.db import models


class Base(models.Model):
    create = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class User(Base):
    name = models.CharField(max_length=120)
    social_security_number = models.CharField(max_length=120)

    def __str__(self):
        return self.name
