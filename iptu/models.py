from django.db import models

from users.models import User


class Base(models.Model):
    create = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class IPTU(Base):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_iptu')
    subscription = models.CharField(max_length=20)
    value = models.CharField(max_length=120)
    area_total = models.IntegerField()
    improvement = models.BooleanField()

    def __str__(self):
        return self.subscription
